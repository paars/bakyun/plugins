package plugins

import "go.uber.org/fx"

var (
	Host Registry

	HostProvider = fx.Provide(GetHost)
)

func GetHost() Registry {
	return Host
}

type Registry interface {
	RegisterFileProvider(id string, provider FileProvider) error
	FileProvider(id string) (FileProvider, error)
}
