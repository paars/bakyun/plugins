package plugins

import (
	"encoding/base64"
	"fmt"
	"hash/adler32"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"sync"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

// LocalFileProvider works with files on the local file system
type LocalFileProvider struct {
	baseURL   *url.URL
	assetPath string

	once sync.Once
}

// Initialize the provider
// base : define a custom base url to work with
// path : define a custom folder path to store files in
func (lf *LocalFileProvider) Initialize(opts map[string]string) error {
	var resultErr error
	lf.once.Do(func() {
		rawURL := opts["base"]
		u, err := url.Parse(rawURL)
		if err != nil {
			resultErr = err
			return
		}
		lf.baseURL = u

		lf.assetPath = "store"
		if ap, ok := opts["path"]; ok {
			lf.assetPath = ap
		}

		if !filepath.IsAbs(lf.assetPath) {
			resolved, _ := filepath.Abs(lf.assetPath)
			log.Warn().Str("assetPath", lf.assetPath).Str("resolved", resolved).Msg("relative assetpath detected")
		}

		if _, err := os.Stat(lf.assetPath); os.IsNotExist(err) {
			resultErr = errors.Wrap(err, "assetpath does not exit")
			return
		}
	})
	return resultErr
}

// Save a io.Reader and return a filePath (or id)
// Optionally allow overwriting files
func (lf *LocalFileProvider) Save(fileName string, reader io.Reader, overwrite bool) (string, int64, error) { // revive:disable-line:flag-parameter
	ext := filepath.Ext(fileName)
	fname := filepath.Base(fileName)
	f, err := ioutil.TempFile(lf.assetPath, fname)
	if err != nil {
		return "", 0, err
	}
	defer func() {
		os.Remove(f.Name())
	}()

	tee := io.TeeReader(reader, f)
	adler := adler32.New()
	written, err := io.Copy(adler, tee)
	if err != nil {
		return "", 0, err
	}
	hash := base64.StdEncoding.EncodeToString(adler.Sum(nil))
	finalName := fmt.Sprintf("%v_%v%v", fname, hash, ext)
	finalPath := filepath.Join(lf.assetPath, fileName)

	if _, err := os.Stat(finalPath); os.IsExist(err) && !overwrite {
		return "", 0, err
	}

	if err := os.Rename(f.Name(), finalPath); err != nil {
		return "", 0, err
	}

	return finalName, written, nil
}

// Delete a file by the given path
func (lf *LocalFileProvider) Delete(filePath string) (bool, error) {
	err := os.Remove(filePath)
	return err == nil, err
}

// Exists checks for file existance
func (lf *LocalFileProvider) Exists(filePath string) (bool, error) {
	_, err := os.Stat(filepath.Join(lf.assetPath, filePath))
	return os.IsExist(err), nil
}

// PublicURL returns the public url to a file
func (lf *LocalFileProvider) PublicURL(filePath string) (publicURL *url.URL, err error) {
	u, err := url.Parse(filePath)
	if err != nil {
		return nil, err
	}

	return lf.baseURL.ResolveReference(u), nil
}
